﻿open System

// Debugging/output
let debug x = printf "%O" x; System.Diagnostics.Debug.Write x

// Data retrieval
let values<'T when 'T : enum<int>> = Enum.GetValues(typeof<'T>) :?> 'T array |> Array.toList

// Basic enums
type Saneness = | Sane = 1 | Insane = 2 // | Unknown = 3
type Person =   | Two = 1 | Three = 2 | Four = 3 | Five = 4 | Six = 5 | Seven = 6 | Ace = 7 | Jack = 8

/// All permutations of persons
let worlds =
    let rec worlds' acc = function
        | []         -> [new Map<_,_>(acc)]
        | person::ps -> [for saneness in values<Saneness> do yield! worlds' ((person, saneness)::acc) ps]
    worlds' []  values<Person>

/// World filtering
let rec apply worlds = function
    | []        -> worlds
    | rule::rs  -> worlds |> Seq.filter rule |> apply <| rs

// DSL specifics:
let ( ~%  ) = Map.find
let (  *  ) person saneness = %person >> ((=) saneness)
let (  *! ) person saneness = %person >> ((<>) saneness)
let join op f1 f2 = fun world -> op (f1 world) (f2 world)
let ( <&> ) = join (&&) 
let ( ==  )  = join (=)

/// predicate defining who says what
let ( := ) person rule world =
    match (%person) world with
    | Saneness.Sane     -> rule world
    | Saneness.Insane   -> not (rule world)
    | _                 -> true // anything is possible for Unknown state

let rules = [
    Person.Three    :=         Person.Ace   * Saneness.Insane
    Person.Four     := not << (Person.Three * Saneness.Insane <&> Person.Two   * Saneness.Insane)
    Person.Five     :=        %Person.Ace == %Person.Four
    Person.Six      :=         Person.Ace   * Saneness.Sane   <&> Person.Two   * Saneness.Sane
    Person.Seven    :=         Person.Five  * Saneness.Insane
    Person.Jack     := not << (Person.Six   * Saneness.Insane <&> Person.Seven * Saneness.Insane)
    // the following is experimenting with tristate
    //                           Person.Two   *! Saneness.Sane
    //                           Person.Five  *! Saneness.Sane
    //                           Person.Seven *! Saneness.Sane
]

let worldsFiltered = apply worlds rules

let answer =
    let answers = worldsFiltered |> Seq.distinctBy %Person.Jack
    match Seq.length answers with
    | 0 -> "No solution"
    | 1 -> sprintf "Success, Jack is %O" (answers |> Seq.head |> %Person.Jack)
    | _ -> "No solution, multiple answers"
    |> (sprintf "ANSWER: %s\n" >> debug)

worlds
|> (Seq.length >> sprintf "debug: Initial permutations %d\n" >> debug)

worldsFiltered
|> (Seq.length >> sprintf "debug: Filtered permutations %d\n" >> debug)

(*
"debug: values for Jack: " |> debug
worlds
|> apply <| rules
|> Seq.iter (
    (%Person.Jack) >> sprintf "%A " >> debug
    )
"\n" |> debug

"debug: all values:\n" |> debug
worlds
|> apply <| rules
|> Seq.iter (fun w ->
    for wi in w do (sprintf "%A " wi) |> debug
    debug "\n"
    )

*)